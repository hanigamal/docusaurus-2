## Who am I

Hey, I’m Hani an experienced consultant, analyst programmer and a human-being who loves to help techies and entrepreneurs find solutions via mentoring, guiding to solve problems and discussing technical viewpoints.
I adore technical challenges with strong experience in software architecture design and high integration problem solving skills gained over 25 years of professional diverse experience ranging from developing business solutions, plans, gathering requirements, specifications, creating user documentation to architectural systems research and enterprises/governments consultancy.  
__The co-founder of PHP for Arabs & PHP Egypt official user-groups.__  
**Specialties:** Extensive knowledge of few programming languages through years of creating enterprise solutions including “Smart Devices” solutions development.

- - - -

## Experience

### Chief Technology Officer
PayCore Egypt  
Jan 2023 - Now  
Hurghada, Egypt  

Established 2022, I'm responsible for knowing which new, potentially disruptive technologies are surfacing, to grasp both the dangers and the opportunities they pose for the firm, and to see and act across the organization to help formulate and execute a company-wide response. Acting CSO.
- - - -

### Independent Business Owner
Maktabak Dot Net  
Sep 2004 - Oct 2022 · 18 yrs  
Cairo, Egypt  

Established 2004, I’ve been managing the firm operations during the past 18 years, handling clients in Egypt, UAE, USA, Mexico, Canada and KSA; Solutions of Maktabak are all in mobile form beside being desktop or web, all apps are distributed varied from reporting management, sales force automation, wireless hot-spot management, inventory control_management, point of sales_service, hardware tracking and documents archiving solutions. In 2014, Maktabak received the World Quality Commitment Award for being influential and active within the community, committed to continuous improvement and have a visible track record of achievement and innovation.
- - - -

### App. Development Manager 
EPL House  
Oct 2018 - Mar 2019 · 6 mos  
Cairo, Egypt  

EPL House is an international software house owned by group of investors, the enterprise specialized in financial and banking solutions.  
Main responsibility was to start up building the company as follows:
- [ ] Planning and developing company’s business & technical strategy for the upcoming 5 years.
- [ ] Develop company’s mission and vision according to business & technical strategy plans.
- [ ] Execute, handle and develop on-hold projects of company’s only client, the Libyan “Bank of commerce and development”
- [ ] Handling company’s technical talent acquisition “HR Operations”

- - - -

### Director Of Operations
IMPACT Technology LTD. IT.SA  
Sep 2016 - Oct 2017 · 1 yr 2 mos  
Cairo, Egypt  

Starting up the company in Egypt from ground zero, started by creating the initial business plan and hiring the team and within one year it was fully operating two major web projects and has team of 20 employees; Primary responsibilities include the following:
- [ ] Hiring, mentoring & operating teams of developers, designers, business_system analysts and QC_QA.
- [ ] Analyze, design, develop and running web solutions including mobile apps, databases and web services applying the latest web and mobile technologies committed to the highest world quality standards.
- [ ] Prospecting new clients in the Middle East market and following up with existing clients from KSA.
- [ ] Creating business plans for existing clients and feasibility studies for their running projects.
- [ ] Achieving the company’s vision CMMI 3 certified within 2 years as per the initial business plan.

- - - -

### Senior Software Engineer
Future Markets Ltd  
May 2015 - Jul 2016 · 1 yr 3 mos  
Cairo, Egypt  

Primary responsibilities include analyze, design and develop a brand new brokerage accounting system for stock-markets trading and other financial services including Forex trading.

- - - -

### Business Development Manager
Trade World Gates Solutions  
Apr 2014 - Mar 2015 · 1 yr  
Cairo, Egypt  

Primary responsibilities include analyze, design and develop new business solutions, plans, gathering requirements, specifications for company teams, update company’s skeleton building powerful teams and hardware import business consultancy.

- - - -

### Development Consultant
ThePlanet  
Mar 2013 - Feb 2014 · 1 yr  
Cairo, Egypt  

Primary responsibilities include upgrade Planet’s development process making it’s own standards applying them to create a new project management system; Upgrade, repair and adjust clients running projects, prepare for the launch of JuiceLabs the Start-up Accelerator.

- - - -

### Senior Programmer & E-Government Consultant
Makkah Emirate Government  
Dec 2009 - Nov 2011 · 2 yrs  
Makkah, Saudi Arabia  

Primary responsibilities include design, development and integration of the following solutions:
- [ ] Porting and migration from Oracle forms to PHP 5 (without altering the database)
- [ ] Business Process Management, with an important addition of re-establishing the logical process of passing daily processes across the kingdom.
- [ ] Human Resources Management, with an important addition of securing the system with a different isolated methods of security.
- [ ] Inventory Control Management, with an important addition of managing dispatched items (Weapons_Arms_Ammunition) between ministry’s warehouses across the kingdom.
- [ ] SMS management server and application to send/receive official data and special occasions messages.
Analyze, design and develop the following solutions (from scratch) as a part of E-Government system:
- [ ] Fixed Assets Management as a part of E-Government system, to manage the tracking of every fixed asset (personal computers and it’s accessories, chairs, desks, printers, etc.)
- [ ] Meeting Organizer with entry permissions and a record set of visitors’ data.
- [ ] Customer Service Ticketing System (kiosk machines, barcode printers/scanners, HD monitors and video ads system) completely integrated with Business Process Management.
- [ ] HTML5, jQuery mobile application for tracking business processes integrated with SMS.

- - - -

### IT Infrastructure Manager
Germanischer Lloyd AG  
Sep 2008 - Aug 2009 · 1 yr  
Abu Dhabi, United Arab Emirates  

Primary responsibilities include the following: 
- [ ] Managing infrastructure & domain controller.
- [ ] Test and evaluate power-builder solutions which designed and developed by GL Malaysia.
- [ ] Design, develop and test a complete web application to manage company operations HR, Projects, Activities, Field Reporting, Document Management and Correspondence System.

- - - -

### Mobility Development Department Manager
PlusPoint GmbH  
Sep 2007 - Aug 2008 · 1 yr  
Dubai, United Arab Emirates  

Primary responsibilities include design, development and “Smart Devices” adaptation of the following solutions:
- [ ] Mobile Restaurant Management
- [ ] Mobile Mini-Bar Stock Control
- [ ] Point of sale system
- [ ] Stock Control Software for Hotels and Lodges
- [ ] Comprehensive Reporting and BI tools

- - - -

### Sr. Programmer & Software Department Manager
BMB Group  
Sep 2005 - Aug 2007 · 2 yrs  
Cairo, Egypt  

Primary responsibilities include design and development of enterprise solutions, R & D duties and core native changes to Windows CE for Arabic language support.
- [ ] Complete design and development of the following dual language solutions:
- [ ] Mobile Asset Tracking
- [ ] Mobile Merchandising Survey
- [ ] Mobile Price Checker Barcode Print
- [ ] Mobile Money Collection (SAT FORMS) (Palm & Symbian)
- [ ] Developed and tested new financial reporting system for the Bad Debts Department of Proserv Express (VB6, SQL Server, Crystal Reports)
- [ ] Mobile Database integration suite for synchronization
Additional tasks:
- [ ] Support clients on spot in the following fields:
- [ ] Wireless networks survey and access
- [ ] Products barcode classification setting up and printing
- [ ] Training on software/hardware

- - - -

### Senior Programmer
Datex Corporation  
Sep 2004 - Aug 2005 · 1 yr  
Cairo, Egypt  

Primary responsibilities include design, programming and integration of enterprise supply chain management solutions.
- [ ] Complete design and development of the following dual language solutions:
- [ ] Mobile Sales Force Automation (VB.NET, SQL Server, SOAP, XML)
- [ ] Inventory Control (C#.NET, SQL CE, SOAP, XML)
- [ ] Workforce Management (C#.NET, SQL CE, SOAP, XML)
- [ ] Mobile Warehouse Management Integration (Remote Emulation)  
Additional Tasks:
- [ ] Development of web applications, scene animation and creating pre-sales presentations.

- - - -
## Education
University of Phoenix  
Master’s degree, Computer and Information Systems  
Security/Information Assurance  
2007 - 2010
- - - -
Helwan University Cairo  
Bachelor’s degree, Touristic Guidance  
1997 - 2001

